package com.example.android.kotlinyjava;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by julionava on 2/23/18.
 */

public class ListadoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listado);

        TextView textView = findViewById(R.id.txtListadoUser);
        if(getIntent() != null && getIntent().hasExtra("user")) {
            textView.setText("Bienvenido : " + getIntent().getExtras().getString("user"));
        }

        RecyclerView rcListado = (RecyclerView) findViewById(R.id.rcListado);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rcListado.setLayoutManager(linearLayoutManager);

        rcListado.setAdapter( new AdapterListado(getFakeData()) );

    }

    public List<PojoBasico> getFakeData() {
        List<PojoBasico> fakeData = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            fakeData.add(new PojoBasico("Titulo", "Desc"));
        }

        return fakeData;
    }
}
