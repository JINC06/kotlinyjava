package com.example.android.kotlinyjava

import android.content.SharedPreferences

/**
 * Created by julionava on 2/24/18.
 */


fun SharedPreferences.edit(func:SharedPreferences.Editor.() -> Unit){
    val editor = edit()
    editor.func()
    editor.apply()
}