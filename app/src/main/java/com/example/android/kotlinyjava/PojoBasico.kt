package com.example.android.kotlinyjava

/**
 * Created by julionava on 2/23/18.
 */

//Agregando esa etiqueta permite construir el objecto con solo un parametro

data class PojoBasico @JvmOverloads constructor(val titulo:String, val desc:String = "123")

