package com.example.android.kotlinyjava

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast

/**
 * Created by julionava on 2/22/18.
 */

class MainActivity : AppCompatActivity() {

    private val TAG = this.javaClass.simpleName
    lateinit var edUsuario: EditText
    lateinit var edContra: EditText
    lateinit var btnLogin: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        edUsuario = findViewById(R.id.edtLoginUsuario)
        edContra = findViewById(R.id.edtLoginContrasenia)
        btnLogin = findViewById(R.id.btnLogin)

        val sharedPreferences = getSharedPreferences("kotlinprefs", Context.MODE_PRIVATE)

        val isLogged = sharedPreferences.getBoolean("loggeado", false)

        if(isLogged) {
            startActivity(Intent(this@MainActivity, ListadoActivity::class.java))
            finish()
        }

        btnLogin.setOnClickListener {
            val isValid =  edContra.passwordValida() //Validaciones.passwordValida(edContra!!)
            Log.e(TAG, "onClick: " + isValid)
            if (isValid) {
                //startActivity(Intent(this@MainActivity, ListadoActivity::class.java))
                val intent = Intent(this@MainActivity, ListadoActivity::class.java)
                intent.putExtra("user", edUsuario.text.toString())
                startActivity(intent)
                sharedPreferences.edit {
                    putBoolean("loggeado", true)
                }
            } else {
                Toast.makeText(this@MainActivity, "Password Invalida", Toast.LENGTH_SHORT).show()
            }
        }

        //        PojoBasico pojoBasico = new PojoBasico("Titulo", "Descripcion");
        //        PojoBasico pojoBasico2 = new PojoBasico("Titulo");
        //
        //        Log.d(TAG, pojoBasico.toString());
        //        Log.d(TAG, pojoBasico2.toString());
        //
        //        String descripcion = pojoBasico2.getDesc();
        //        Log.e(TAG, "Descripcion " + descripcion);
    }
}
