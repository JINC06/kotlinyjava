package com.example.android.kotlinyjava

import android.widget.EditText

/**
 * Created by julionava on 2/22/18.
 */

fun EditText.passwordValida():Boolean{
    return this.text.toString().isNotEmpty() &&
            this.text.toString().length.compareTo(6) == 1
}
