package com.example.android.kotlinyjava;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

/**
 * Created by julionava on 2/24/18.
 */

public class AdapterListado extends RecyclerView.Adapter<AdapterListado.ViewHolder> {

    private List<PojoBasico> list;

    public AdapterListado(List<PojoBasico> list) {
        this.list = list;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        PojoBasico data = list.get(position);

        holder.tvTitle.setText( data.getTitulo() );
        holder.tvDesc.setText( data.getDesc() );
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_lista, parent, false));
    }
    @Override
    public int getItemCount() {
        return list.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        TextView tvTitle;
        TextView tvDesc;

        public ViewHolder(View itemView) {
            super(itemView);

            tvTitle = itemView.findViewById(R.id.txtItemTitle);
            tvDesc = itemView.findViewById(R.id.txtItemDesc);
        }

    }
}

